# Bitmap exercise

An attempt at the bitmap exercise. The logic in Canvas is not exactly pretty! 

## Notes
* I wrote some example tests, but these would need expanding
* The logic in Canvas needs serious work, it's not exactly dry or consistent
* I decided to package the task in a Gem
* Invalid commands should be accounted for
* Rudimentary help system in place

## Install
Install with `bundle install`

## Running
Simply run with `./bin/bitmap` from within the project root.

## Testing
Run the tests with `bundle exec rspec`

# Author
[julian@juco.co.uk](http://wwww.juco.co.uk)
