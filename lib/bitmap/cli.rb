module Bitmap
  class Cli

    def listen
      greet
      while true do
        prompt
        parse_input gets
      end
    end

    private
      def greet
        puts "Hello! Welcome to the bitmap generator\n\n"
      end

      def prompt
        print '>> '
      end

      def parse_input(input)
        input = input.split ' '

        command = Command::Factory.get(input.shift.upcase)

        input = command.parse_numerics(input) if command.respond_to?(:parse_numerics)
        if command.respond_to?(:valid_command?) && !command.valid_command?(input)
          raise ArgumentError.new(command.help)
        end

        command.method(:execute).call(input)
      rescue CommandNotFoundError
        puts 'Unknown command. Please try again'
      rescue CanvasNotInitializedError
        puts 'The canvas must be initialized first!'
      rescue ArgumentError => e
        puts "Invalid usage! Usage: #{e.message}"
      end

  end
end
