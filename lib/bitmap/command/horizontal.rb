module Bitmap
  module Command
    class Horizontal
      include Validator
      include Sanitizer

      def execute(params)
        x1, x2, y, color = params
        Bitmap::Canvas.instance.color_horizontal(x1, x2, y, color)
      end

      def valid_command?(params)
        raise CanvasNotInitializedError unless Bitmap::Canvas.instance.initialized?
        super
      end

      def sequence(params)
        [Fixnum, Fixnum, Fixnum, String]
      end

      def help
        "H X-START X-END Y - Draw a horizontal Between X-START and X-END in Y"
      end

    end
  end
end
