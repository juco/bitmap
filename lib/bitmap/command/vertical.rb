module Bitmap
  module Command
    class Vertical
      include Validator
      include Sanitizer

      def execute(params)
        x, y1, y2, color = params
        Bitmap::Canvas.instance.color_vertical(x, y1, y2, color)
      end

      def valid_command?(params)
        raise CanvasNotInitializedError unless Bitmap::Canvas.instance.initialized?
        super
      end

      def sequence
        [Fixnum, Fixnum, Fixnum, String]
      end

      def help
        'I X Y-START Y-END COLOR - Draw a vertical line on Y between X-START'\
        'and X-END'
      end

    end
  end
end
