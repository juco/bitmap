module Bitmap
  module Command
    module Sanitizer

      def parse_numerics(params)
        params.map do |param|
          is_integer?(param) ? param.to_i - 1 : param
        end
      end

      private
        def is_integer?(param)
          return true if param.is_a? Fixnum
          return true if param.respond_to?(:is_number?) && param.is_number?
          false
        end

    end
  end
end
