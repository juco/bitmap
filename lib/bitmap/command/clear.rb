module Bitmap
  module Command
    class Clear

      def execute(params)
        Bitmap::Canvas.instance.wipe
      end

      def valid_command?(params)
        raise CanvasNotInitializedError unless Bitmap::Canvas.instance.initialized?
      end

    end
  end
end
