module Bitmap
  module Command
    module Validator

      def valid_command?(params)
        return true unless self.respond_to? :sequence
        valid_length   = params.length == sequence.length
        valid_sequence = params.map(&:class) == sequence
        valid_length and valid_sequence
      end

    end
  end
end
