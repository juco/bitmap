module Bitmap
  module Command
    class Range
      include Validator
      include Sanitizer

      def execute(params)
        x, y, color = params
        Bitmap::Canvas.instance.color_range(x, y, color)
      end

      def valid_command?(params)
        raise CanvasNotInitializedError unless Bitmap::Canvas.instance.initialized?
        super
      end

      def sequence
        [Fixnum, Fixnum, String]
      end

      def help
        "F X Y COLOR - Fill the range of X and Y"
      end

    end
  end
end
