module Bitmap
  module Command
    class Show

      def execute(params)
        Bitmap::Canvas.instance.print
      end

      def valid_command?(params)
        raise CanvasNotInitializedError unless Bitmap::Canvas.instance.initialized?
        true
      end

    end
  end
end
