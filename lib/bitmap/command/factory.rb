module Bitmap
  module Command
    module Factory

      MAP = {
        'I' => Init,
        'L' => Pixel,
        'V' => Vertical,
        'H' => Horizontal,
        'F' => Range,
        'C' => Clear,
        'S' => Show,
        'X' => Exit
      }

      def self.get(key)
        raise CommandNotFoundError unless MAP[key]
        MAP[key].new
      end

    end
  end

  class CommandNotFoundError < ArgumentError; end;
end
