module Bitmap
  module Command
    class Exit

      def execute(params)
        puts 'Have a great day!'
        exit 0
      end

    end
  end
end
