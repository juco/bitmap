module Bitmap
  module Command
    class Pixel
      include Validator
      include Sanitizer

      def execute(params)
        x, y, color = params
        Bitmap::Canvas.instance.set_pixel(Bitmap::Pixel.new(x, y, color))
      end

      def valid_command?(params)
        raise CanvasNotInitializedError unless Bitmap::Canvas.instance.initialized?
        super
      end

      def sequence
        [Fixnum, Fixnum, String]
      end

      def help
        "L X Y COLOR - Color in the pixel at X & Y"
      end

    end
  end
end

