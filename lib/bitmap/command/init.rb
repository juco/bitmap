module Bitmap
  module Command
    class Init

      def execute(params)
        x, y = params.map(&:to_i)
        Bitmap::Canvas.instance.init(x, y)
      end

      def valid_command?(params)
        params.select { |p| p.respond_to? :is_number? }.length == 2
      end

      def help
        "I X Y - Initialize a canvas of X and Y"
      end

    end
  end
end
