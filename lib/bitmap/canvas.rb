module Bitmap
  class Canvas
    include Singleton

    WHITE = 'O'

    attr_reader :canvas

    def init(column_count, row_count, base_color = WHITE)
      @column_count = column_count
      @row_count = row_count

      @canvas = Array.new(column_count) do |col|
        Array.new(row_count) do |row|
          Pixel.new(col, row, base_color)
        end
      end
    end

    def set_pixel(pixel)
      @canvas[pixel.y][pixel.x] = pixel
    end

    def color_horizontal(x_start, x_end, y, color)
      puts "Called with: #{x_start} #{x_end}, #{y}, #{color}"
      @canvas[y].fill(x_start..x_end) do |x|
        Bitmap::Pixel.new(x, y, color)
      end
    end

    def color_vertical(x, y_start, y_end, color)
      @canvas[y_start..y_end].each_with_index.map do |pixel, i|
        pixel.fill(Pixel.new(x, i, color), x, 1)
      end
    end

    def color_range(x, y, color)
      old_pixel = pixel(x, y)
      new_pixel = Bitmap::Pixel.new(x, y, color)
      set_pixel(new_pixel)
      map do |p|
        if p.color == old_pixel.color
          Bitmap::Pixel.new(p.x, p.y, color)
        else
          p
        end
      end
    end

    def map
      @canvas = @canvas.map do |row|
        row.map do |pixel|
          yield(pixel)
        end
      end
    end

    def pixel(x, y)
      @canvas[y][x]
    end

    def wipe
      init(@column_count, @row_count)
    end

    def initialized?
      @row_count and @column_count
    end

    def to_s
      return 'Uninitialized canvas' unless @canvas
      @canvas.each do |row|
        puts row.join(' ')
      end
    end
    alias :print :to_s
  end

  class CanvasNotInitializedError < StandardError; end;
end
