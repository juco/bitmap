module Bitmap
  class Pixel

    attr_reader :x, :y
    attr_accessor :color

    def initialize(x, y, color)
      @x = x.to_i
      @y = y.to_i
      @color = color
    end

    def borders?(pixel)
      borders_x = pixel.y == y && (pixel.x == x - 1 || pixel.x == x + 1)
      borders_y = pixel.x == x && (pixel.y == y - 1 || pixel.y == y + 1)
      pixel.color == color and (borders_x or borders_y)
    end

    def to_s
      color
    end

  end
end
