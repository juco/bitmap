require 'singleton'

require 'bitmap/version'
require 'bitmap/ext/string'

Dir[(File.join(File.dirname(__FILE__), 'bitmap', 'ext', '*.rb'))].each { |f| puts f }

require 'bitmap/pixel'
require 'bitmap/canvas'

require 'bitmap/command/validator'
require 'bitmap/command/sanitizer'
require 'bitmap/command/init'
require 'bitmap/command/pixel'
require 'bitmap/command/vertical'
require 'bitmap/command/horizontal'
require 'bitmap/command/range'
require 'bitmap/command/clear'
require 'bitmap/command/show'
require 'bitmap/command/exit'
require 'bitmap/command/factory'

require 'bitmap/cli'

module Bitmap
  def self.run
    Cli.new.listen
  end
end
