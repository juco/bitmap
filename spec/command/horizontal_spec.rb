require 'spec_helper'

describe 'Horizontal command', horizontal: true do
  before :each do
    @canvas = Bitmap::Canvas.instance
    allow(@canvas).to receive(:color_horizontal)
  end

  it 'should sanitize correctly' do
    Bitmap::Command::Horizontal.new.execute([1, 2, 3, 'X'])
    expect(@canvas).to have_received(:color_horizontal).with(1, 2, 3, 'X')
  end

  it 'should not work' do
    Bitmap::Command::Horizontal.new.execute('2')
  end
end
