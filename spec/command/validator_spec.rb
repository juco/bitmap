require 'spec_helper'

describe 'Bitmap command validator', validator: true do
  describe 'valid_command? method' do
    let(:dummy_class) do
      Class.new do
        include Bitmap::Command::Validator
        def sequence; [Fixnum, Fixnum]; end;
      end
    end

    it 'should return true for valid commands' do
      expect(dummy_class.new.valid_command?([5, 5])).to be true
    end

    it 'should return false for an incorrect length of params' do
      expect(dummy_class.new.valid_command?([1])).to be false
      expect(dummy_class.new.valid_command?([1, 2, 3])).to be false
    end

    it 'should return false incorrect param types' do
      expect(dummy_class.new.valid_command?([1, 'two'])).to be false
    end
  end
end
