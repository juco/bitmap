require 'spec_helper'

describe 'Sanitizer spec' do
  let(:dummy_class) { Class.new { include Bitmap::Command::Sanitizer } }

  describe 'parse_numerics method' do
    it 'should decrement number params' do
      expect(dummy_class.new.parse_numerics([1, 2])).to eq [0, 1]
    end

    it 'should decremement number params when there\'s a mixture' do
      expect(dummy_class.new.parse_numerics([1, 'two', 3])).to eq [0, 'two', 2]
    end
  end
end
