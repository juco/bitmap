describe 'String extension' do
  it 'is_number? should return true for numeric strings' do
    expect('2'.is_number?).to be true
    expect('99'.is_number?).to be true
  end

  it 'is_number? should return false for non numerics' do
    expect('x9'.is_number?).to be false
    expect('qq'.is_number?).to be false
  end
end
