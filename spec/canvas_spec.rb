require 'spec_helper'

describe 'Canvas' do
  it 'should respond to instance' do
    expect(Bitmap::Canvas.respond_to? :instance).to be true
  end

  it 'should return the same canvas' do
    Bitmap::Canvas.instance.init(5, 5)
    expect(Bitmap::Canvas.instance.canvas.length).to be 5
  end

  it 'should init a canvas with the correct size' do
    Bitmap::Canvas.instance.init(10, 15)
    expect(Bitmap::Canvas.instance.canvas.length).to eq 10
    expect(Bitmap::Canvas.instance.canvas[2].length).to eq 15
  end

  it 'should init a white canvas by default' do
    Bitmap::Canvas.instance.init(5, 5)
    expect(Bitmap::Canvas.instance.canvas[1][1].color).to be Bitmap::Canvas::WHITE
  end

  it 'should provide the ability to color an individual pixel' do
    Bitmap::Canvas.instance.init(5, 5, 'X')
    pixel = Bitmap::Pixel.new(2, 2, 'W')
    Bitmap::Canvas.instance.set_pixel(pixel)
    expect(Bitmap::Canvas.instance.canvas[2][2].color).to eq 'W'
  end

  it 'should provide the ability to color horizontally' do
    Bitmap::Canvas.instance.init(5, 5, 'Q')
    Bitmap::Canvas.instance.color_horizontal(2, 3, 2, 'X')
    expect(Bitmap::Canvas.instance.pixel(2, 2).color).to eq 'X'
  end

  it 'should provide the ability to color vertically' do
    Bitmap::Canvas.instance.init(10, 10, 'R')
    Bitmap::Canvas.instance.color_vertical(4, 2, 5, 'X')
    expect(Bitmap::Canvas.instance.pixel(4, 3).color).to eq 'X'
  end

  it 'should fill a range' do
    Bitmap::Canvas::instance.init(10, 10, 'W')
    Bitmap::Canvas.instance.color_vertical(3, 2, 9, 'X')
    Bitmap::Canvas.instance.color_range(3, 4, 'Q')
    expect(Bitmap::Canvas.instance.pixel(3, 3).color).to eq 'Q'
    expect(Bitmap::Canvas.instance.pixel(3, 5).color).to eq 'Q'
  end
end
