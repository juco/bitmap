require 'spec_helper'

describe 'Pixel' do

  it 'should initialize with coordinates and color' do
    pixel = Bitmap::Pixel.new(5, 5, 'O')
    expect(pixel.x).to be 5
    expect(pixel.y).to be 5
    expect(pixel.color).to eq 'O'
  end

  it 'should allow for a change in color' do
    pixel = Bitmap::Pixel.new(2, 3, 'O')
    pixel.color = 'X'
    expect(pixel.color).to eq 'X'
  end

  it 'should print as the color' do
    pixel = Bitmap::Pixel.new(1, 1, 'Z')
    expect(pixel.to_s).to eq 'Z'
  end

  it 'should correctly identify bordered pixels' do
    pixel = Bitmap::Pixel.new(2, 2, 'X')
    expect(pixel.borders? Bitmap::Pixel.new(2, 1, 'X')).to be true
    expect(pixel.borders? Bitmap::Pixel.new(3, 2, 'X')).to be true
    expect(pixel.borders? Bitmap::Pixel.new(2, 3, 'X')).to be true
    expect(pixel.borders? Bitmap::Pixel.new(3, 3, 'X')).to be false
    expect(pixel.borders? Bitmap::Pixel.new(2, 1, 'Z')).to be false
    expect(pixel.borders? Bitmap::Pixel.new(2, 2, 'x')).to be false
  end
end
